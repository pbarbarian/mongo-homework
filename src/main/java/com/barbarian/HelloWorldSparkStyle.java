package com.barbarian;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

/**
 * Minimal GET controller even without a page
 * Created by barbarian on 3/21/16.
 */
public class HelloWorldSparkStyle {
    public static void main(String[] args) {
        Spark.get(new Route("/") {
            @Override
            public Object handle(Request request, Response response) {
                return "Hello World from Spark";
            }
        });
    }
}
