package com.barbarian;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Suggests you to select your favorite fruit. Has a GET & POST controller
 * Created by barbarian on 3/21/16.
 */
public class SparkFormHandling {
    public static void main(String[] args) {
        final Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(
                SparkFormHandling.class, "/");

        Spark.get(new Route("/") {
            @Override
            public Object handle(Request request, Response response) {
                Map<String, Object> fruitMap = new HashMap<String, Object>();
                fruitMap.put("fruits", Arrays.asList("apple", "orange", "banana", "peach"));
                try {
                    Template fruitPickerTemplate =
                            configuration.getTemplate("fruitPicker.ftl");
                    StringWriter writer = new StringWriter();
                    fruitPickerTemplate.process(fruitMap, writer);
                    return writer;
                } catch (Exception e) {
                    halt(500);
                    e.printStackTrace();
                }
                return null;
            }
        });

        Spark.post(new Route("/favourite_fruit") {
            @Override
            public Object handle(Request request, Response response) {
                final String fruit = request.queryParams("fruit");
                if (fruit == null) {
                    return "Why don't you pick one?";
                } else {
                    return "Your favourite fruit is " + fruit;
                }
            }
        });
    }
}
